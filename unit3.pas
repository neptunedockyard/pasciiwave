unit Unit3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TAboutWindow }

  TAboutWindow = class(TForm)
    AboutCloseBtn: TButton;
    Memo1: TMemo;
    procedure AboutCloseBtnClick(Sender: TObject);
  private

  public

  end;

var
  AboutWindow: TAboutWindow;

implementation

{$R *.lfm}

{ TAboutWindow }

procedure TAboutWindow.AboutCloseBtnClick(Sender: TObject);
begin
  AboutWindow.Close;
end;

end.

