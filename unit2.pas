unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TSignalWindow }

  TSignalWindow = class(TForm)
    SigOKBtn: TButton;
    SigCancelBtn: TButton;
    SigNameBox: TEdit;
    SigPatternBox: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure SigOKBtnClick(Sender: TObject);
    procedure SigCancelBtnClick(Sender: TObject);
  private

  public
    sigName: String;
    sigPattern: String;
    sigReplace: Boolean;
  end;

var
  SignalWindow: TSignalWindow;

implementation

{$R *.lfm}

{ TSignalWindow }

procedure TSignalWindow.SigOKBtnClick(Sender: TObject);
begin
  sigName := SigNameBox.Text;
  sigPattern := SigPatternBox.Text;
  if (sigName = '') or (sigPattern = '') then begin
    ShowMessage('Please enter a name and pattern.');
  end else begin
    ModalResult:= mrOk;
  end;
end;

procedure TSignalWindow.FormActivate(Sender: TObject);
begin
  ModalResult := 0;
  SigNameBox.SetFocus;
end;

procedure TSignalWindow.SigCancelBtnClick(Sender: TObject);
begin
  ModalResult:= mrCancel;
end;

end.

