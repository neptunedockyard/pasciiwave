unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Menus, Unit2, Unit3;
type

  { TMainWindow }

  TMainWindow = class(TForm)
    waveBox: TListBox;
    inputBox: TListBox;
    MainMenu1: TMainMenu;
    RMenuAdd: TMenuItem;
    MenuItem_Exit: TMenuItem;
    MenuItem_Select: TMenuItem;
    MenuItem_About: TMenuItem;
    RMenuEdit: TMenuItem;
    RMenuDel: TMenuItem;
    FileMenu: TMenuItem;
    EditMenu: TMenuItem;
    HelpMenu: TMenuItem;
    MenuItem_New: TMenuItem;
    MenuItem_Open: TMenuItem;
    MenuItem_Save: TMenuItem;
    Panel1: TPanel;
    PopupMenu1: TPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure inputBoxDblClick(Sender: TObject);
    procedure MenuItem_ExitClick(Sender: TObject);
    procedure MenuItem_SelectClick(Sender: TObject);
    procedure MenuItem_AboutClick(Sender: TObject);
    procedure RMenuAddClick(Sender: TObject);
    procedure RMenuEditClick(Sender: TObject);
    procedure RMenuDelClick(Sender: TObject);
    procedure MenuItem_NewClick(Sender: TObject);
    procedure MenuItem_OpenClick(Sender: TObject);
    procedure MenuItem_SaveClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    function SplitText(const aDelimiter,s: String): TStringList;
    function GenerateAscii(const pattern: String): TStringList;
  private
    res: integer;
  public

  end;

var
  MainWindow: TMainWindow;

implementation

{$R *.lfm}

{ TMainWindow }

procedure TMainWindow.FormCreate(Sender: TObject);
begin
  {* create stuff here i guess *}
  inputBox.Items.Add('Name'^I'Signal');
end;

procedure TMainWindow.inputBoxDblClick(Sender: TObject);
begin
  if (inputBox.ItemIndex <> -1) then begin
    writeln('edit mode');
    MainWindow.RMenuEditClick(sender);
  end else begin
    writeln('add mode');
    MainWindow.RMenuAddClick(sender);
  end;
end;

procedure TMainWindow.MenuItem_ExitClick(Sender: TObject);
begin
  MainWindow.Close;
end;

procedure TMainWindow.MenuItem_SelectClick(Sender: TObject);
begin

end;

procedure TMainWindow.MenuItem_AboutClick(Sender: TObject);
begin
  AboutWindow.Show;
end;

procedure TMainWindow.RMenuAddClick(Sender: TObject);
var
  sigN: String;
  sigP: String;
  newSig: String;
  asciiSig: TStringList;
begin
  SignalWindow.SigNameBox.Text:='';
  SignalWindow.SigPatternBox.Text:='';
  res := SignalWindow.ShowModal;
  if res = mrOk then
    begin
      SignalWindow.sigReplace := False;
      sigN := SignalWindow.sigName;
      sigP := SignalWindow.sigPattern;
      newSig := Concat(sigN,^I,sigP);
      if(SignalWindow.sigReplace) then
      begin
          inputBox.Items[inputBox.ItemIndex] := newSig;
      end else begin
          inputBox.Items.Add(newSig);
      end;
      //asciiSig := GenerateAscii(sigP);
    end else begin
      writeln(res);
    end;
end;

procedure TMainWindow.RMenuEditClick(Sender: TObject);
var
  inputs: TStringList;
  sigN: String;
  sigP: String;
  newSig: String;
begin
  if(inputBox.ItemIndex > 0) then begin
    inputs := SplitText(#9, inputBox.GetSelectedText);
    SignalWindow.SigNameBox.Text:= inputs[0];
    SignalWindow.SigPatternBox.Text:= inputs[1];
    res := SignalWindow.ShowModal;
    if res = mrOk then
      begin
        SignalWindow.sigReplace := True;
        sigN := SignalWindow.sigName;
        sigP := SignalWindow.sigPattern;
        newSig := Concat(sigN,^I,sigP);
        if(SignalWindow.sigReplace) then
        begin
            inputBox.Items[inputBox.ItemIndex] := newSig;
        end else begin
            inputBox.Items.Add(newSig);
        end;
      end else begin
        writeln(res);
      end;
  end else begin
    ShowMessage('Please select a signal to modify.');
  end;
end;

procedure TMainWindow.RMenuDelClick(Sender: TObject);
begin

end;

procedure TMainWindow.MenuItem_NewClick(Sender: TObject);
begin

end;

procedure TMainWindow.MenuItem_OpenClick(Sender: TObject);
begin

end;

procedure TMainWindow.MenuItem_SaveClick(Sender: TObject);
begin

end;

procedure TMainWindow.PopupMenu1Popup(Sender: TObject);
begin
  {* Popup menu *}
  writeln('Popup');
end;

function TMainWindow.SplitText(const aDelimiter,s: String): TStringList;
var
  aList: TStringList;
begin
  aList := TStringList.Create;
  aList.LineBreak := aDelimiter;
  aList.Text := s;
  writeln(s);
  SplitText := aList;
end;

function TMainWindow.GenerateAscii(const pattern: String): TStringList;
var
  newPattern: TStringList;
  i: integer;
  Len: integer;
  Arr: array of String;
  newPat0: array of String;
  newPat1: array of String;
  newPat2: array of String;
begin
  newPattern := TStringList.Create;
  Len := length(pattern);
  SetLength(Arr, Len);
  SetLength(newPat0, Len);
  SetLength(newPat1, Len);
  SetLength(newPat2, Len);

  for i:=0 to Len do
  begin
    Arr[i-1] := pattern[i];
    writeln(pattern[i]);
  end;

  for i:=0 to length(Arr) do
  begin
    case pattern[i] of
      '0': begin
        newPat0[i] := '';
        newPat1[i] := '';
        newPat2[i] := '';
      end;
      '1': begin
        newPat0[i] := '';
        newPat1[i] := '';
        newPat2[i] := '';
      end;
      'x': begin
        newPat0[i] := '';
        newPat1[i] := '';
        newPat2[i] := '';
      end;

    end;
  end;

  GenerateAscii := newPattern;
end;

end.

